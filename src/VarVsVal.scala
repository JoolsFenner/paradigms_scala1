//http://stackoverflow.com/questions/1791408/what-is-the-difference-between-a-var-and-val-definition-in-scala

object VarVsVal {
  def main(args: Array[String]){
    
    class A(n: Int) {
      var value = n
    }

    class B(n: Int) {
      val value = new A(n)
    }

    
    val x = new B(5)
    println(x.value.value)
   
   // x = new B(6) // Doesn't work, because I can't replace the object created on the line above with this new one.
   // x.value = new A(6) // Doesn't work, because I can't replace the object assigned to B.value for a new one.
    x.value.value = 6 // Works, because A.value can receive a new object.
    println(x.value.value)
  }
}
