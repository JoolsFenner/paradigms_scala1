package exam

import scala.collection.mutable.ListBuffer

object wc_currying {
 
	def sum(f: Int => Int): (Int, Int) => Int = {
		def sumF(a: Int, b: Int): Int =
			if (a > b)
				0
			else
				f(a) + sumF(a + 1, b)
			sumF
	}                                         //> sum: (f: Int => Int)(Int, Int) => Int
 
 
	 val sumSq = ((x:Int) => x*x)             //> sumSq  : Int => Int = <function1>
	 
	 val sumCb = ((x:Int) => x*x*x)           //> sumCb  : Int => Int = <function1>
	 
	 
	 sum(sumSq)(1,3)                          //> res0: Int = 14
	 sum(sumCb)(1,3)                          //> res1: Int = 36
	 
 	//In our example, sum(x => x * x)(1, 10) is equivalent to the following expression:
	//(sum(x => x * x))(1, 10).
	 
	// The style of function-returning functions is so useful that Scala has special syntax
	// for it. For instance, the next definition of sum is equivalent to the previous one, but
	// is shorter:
	
	def sum2(f: Int => Int)(a: Int, b: Int): Int =
		if (a > b) 0 else f(a) + sum(f)(a + 1, b)
                                                  //> sum2: (f: Int => Int)(a: Int, b: Int)Int
 	
 	val test_partial = sum2(sumSq)_           //> test_partial  : (Int, Int) => Int = <function2>
 	val test_full = sum2(sumSq)(1,10)         //> test_full  : Int = 385
 	
 	
 	
 	
 	//	CLOSURES
 	// http://stackoverflow.com/questions/36636/what-is-a-closure
 	//  Toy example of a function that knows if its "seen" an integer before
 	//  just to show how the intList memory strcture works
 	def noDupes: Int => String ={
 		
 		//	this is the function's memory
 		//  key point is that when function is assigned to val then this memory structure 'persists' 
 		// "A closure is a persistent local variable scope"
 		val intList = new ListBuffer[Int]()
 		
 		//this is the function that's returned
 		(x: Int) => {
 			intList.contains(x) match {
 				case true  => "I've seen " + x + " before"
 				case false => intList += x
 										"Adding " + x + " to the list"
 			}
 		}
	}                                         //> noDupes: => Int => String
	
	
	
	
	val x =  noDupes                          //> x  : Int => String = <function1>
	//nodupes has now been evaluated and assigned to x
	
	println(x(1))                             //> Adding 1 to the list
	println(x(1))                             //> I've seen 1 before
	println(x(1))                             //> I've seen 1 before
	println(x(2))                             //> Adding 2 to the list
	println(x(2))                             //> I've seen 2 before
	println(x(3))                             //> Adding 3 to the list
	println(x(4))                             //> Adding 4 to the list
 	
 	
 	
 	
}