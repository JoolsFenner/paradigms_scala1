package exam


object Memoization extends App{
  
  
  //memoization
  
  def memo[A, B](f: A => B): A => B = {
    val cache = collection.mutable.Map[A, B]()
    (a: A) => {
      cache get a match {
        case Some(b) => b
        case None =>
          val b = f(a)
          cache(a) = b
          b
      }
    }
  }
  
  
  val memolength = memo {
      xs: List[Int] =>
      println("Computing with memo...")
      xs.length
  }                              //memolength  : List[Int] => Int = <function1>
  
  
  //NOTE THAT "COMPUTING..." ONLY PRINTS ONCE
  memolength(List(1,2,3,4,5,6,7))
  memolength(List(1,2,3,4,5,6,7))
  
  val memolengthNonMemo = {
      xs: List[Int] =>
      println("Computing without memo...")
      xs.length
  } 
  
  //WHEREAS HERE "COMPUTING..." PRINTS TWICE
   memolengthNonMemo(List(1,2,3,4,5,6,7))
   memolengthNonMemo(List(1,2,3,4,5,6,7))
  
  
  
   
  
}