package exam

object FunctionToFunction {

	/*
	Given
		f: A => B
		g: B => C
	How do you get
	  A => C
	
	*/
	  
  println("Hi")                                   //> Hi
  //def answer = [A,B,C](f: A => B, g: B=> C) => (x: A) => g(f(x))
  (f: Int => Double, g: Double => String) => (x: Int) => g(f(x))
                                                  //> res0: (Int => Double, Double => String) => Int => String = <function2>

}