object IOExample {
 def main(args: Array[String]){
   import java.io._
   import scala.io.Source
   val file = new File("testio.txt")
   val writer = new PrintWriter(file)
   writer write "first\r\n"
   writer write "second"
   writer.close()
   val lines = Source.fromFile(file).getLines().toList
   println(lines) 
 }
}