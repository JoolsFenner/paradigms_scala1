package src

object ScalaLabSheet2_Q3 {
  def main(args: Array[String]){
      
    //basic fib implementation, runs in exponential time
      def fibexample(n: Int ): Int = if (n <= 1) 1 else fibexample (n -1) + fibexample (n -2)
      
      
      /*
       * (a) Write a recursive Scala function with the following signature:
          def fib2 (n: Int ): (Int , Int )
          that computes both fib(n) and fib(n-1) in linear time. The function should
          evaluate as follows:
          fib2 (0) = (1 ,1)
          fib2 (1) = (1 ,1)
          fib2 (2) = (2 ,1)
          fib2 (3) = (3 ,2)
          fib2 (4) = (5 ,3)
      */ 
      
      def fib2 (n: Int ): (Int , Int )={
        if(n <= 1) (1,1)
        else{
          val (x, x1) = fib2(n-1)
          (x+x1,x)
        }
      }
      
      //call is using fib, right side takes first element of the returned tuple
      def fib(n: Int) = fib2(n)._1
       
      println(fib(49))
      
      
      //using tail recursion and accumulator
      def fib3(n: Int, acc: Int, acc1: Int): (Int, Int) = {
        if (n <= 1) {
          (acc, acc1)
        } else {
           fib3(n-1, acc + acc1, acc)
        } 
      }
      
       def fibTail(n: Int) = fib3(n,1,1)._1
       
       fibTail(5)
       
       /* Trace of fibTail(5)
        * fib3(5,1,1)
        *   fib3(4,2,1)
        *     fib3(3,3,2)
        *       fib(2,5,3)
        *         fib(1,8,5)
        *           fib(0,13,8)
        */
       
      
    
  }
}