object FunctionalTest {
   def main(args: Array[String]){
     
     /**
      * Understanding foldleft
      * https://coderwall.com/p/4l73-a/scala-fold-foldleft-and-foldright
      * 
      * The fold method for a List takes two arguments; the start value and 
      * a function. This function also takes two arguments; the accumulated 
      * value and the current item in the list. So here's what happens:
      * 
      * At the start of execution, the start value that you passed as the 
      * first argument is given to your function as its first argument. 
      * As the function's second argument it is given the first item on the list 
      * 
      */
     
     val numbers = List(1,2,3,4,5)
     println( numbers.foldLeft(0) { (m: Int, n: Int) => println("m: " + m + " n: " + n)  ; m + n } )
     
     /*returns
      * m: 0 n: 1
        m: 1 n: 2
        m: 3 n: 3
        m: 6 n: 4
        m: 10 n: 5
        15 
      */
     
     //can also write it like this
     
     println(numbers.foldLeft(0) ((m,n) => m+n))
     
     //or
     
     println(numbers.foldLeft(0)(_+_))
   }

}

