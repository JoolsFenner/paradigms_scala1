object BasicSyntax {

  def main(args: Array[String]){
    /**
     * Variables
     */
    
    var x: Int = 5
   // println(x)
    
    var y: Double = 22.3
    //println(y)
    
    /**
     * Arrays
     */
    
    var myArray = Array(11,12,13,14,15,16,17,18,19,20)
    //when no initial values given new is required along with explicit type (paramaterised)
    val ary = new Array[Int](5)
    
    
    /**
     * For loops
     */
    for (i <- 1 to 10){
     // print(i + ", ")
    } 
    
    for (i <- 1 until 10){
     // println(i)
    }
      
    for(x <- myArray){
    //  print(x + ", ")
    }
   // println()
    
    for(x <- myArray if x % 2 == 0){
      //print(x + ", ")
    }
    //println()
    //println("Lists")
    
    /**
     * Lists
     */
    val list2 = List[Int]()
    val list1 = List(45,765,-1,-645)
    
    //println(list1.head)
    //println(list1.tail)
    //println(list1(4))
    
    //illegal (immutable)
    //list1(3)= 4
    
   
   val listAppendValue=  999 :: list1
    
   //println(listAppendValue)
   
   
   val list4 = List(1,2,3)
   val list5 = List(4,5,6)
   
   val listAppendList = list4 ::: list5
   //print(listAppendList)
    
   /**
    * Tuples
    */
    
    //number after Tuple defines size (up to 22)
    val t = Tuple3(33,"Jools",6.3)
    //println(t _1)
    
    /**
     * Maps 
     */
   
     //Maps are a list of tuples
     
    val m  = Map("julian" -> "programmer", "james" -> "banker")
    println( m("julian") )
    
    println(m contains "james")
    
}
}