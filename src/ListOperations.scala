object ListOperations {
  
  //iterative
  def printList1(myList: List[Any]){
    for(i <- 0 until myList.length){
      print(myList(i))
    }
  }
  
  //recursive
  def printList2(myList: List[Any]){
    if(! myList.isEmpty){
      print(myList head)
      printList2(myList tail)
    }
  }
  
  //pattern matching
  def printList3(myList: List[Any]){
    myList match{
      case h :: t =>
         println(h)
         printList3(t)
      case _ =>  
    }
  }
  
  def main(args: Array[String]){
    var x = List(1,2,3,4,5,6)
    //printList1(x)
    //println
    //printList2(x)
    //printList3(x)
    
    
   /**
    * List construction with :: and Nil
    * :: is right-associative
    */
    
    println(1 :: 2 :: 3 :: Nil)
    println(1::(2 :: (3 ::Nil)))
    
  }

}