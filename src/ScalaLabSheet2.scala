object ScalaLabSheet2 {
  
  
  
  
   
  def main (args: Array[String]){
   
   /**
    * Question 1 - Answers
    * 
    */
    
  //(a) A function that takes a Boolean and returns it.
  
  val a = (x: Boolean) => x
  
    
  //(b) A function that takes an Int argument x and returns a function that takes no arguments which returns x.
  
  val b = (x: Int) => () => x
  
 
  
  //(c) A function that takes two arguments, an Int n 
  //and a function f from Int to Int and applies f to n.
  
  val c = (n: Int, f: Int => Int) => f(n)
  
  
  //(d) The curried version of (c).
  
  val d = (n: Int) =>(f: Int => Int) => f(n)
  
  
  //(e) A function that takes an Int n and two functions f and g 
  //from Int to Int and applies f to the result of g applied to n.
  
  val e = (n: Int, f: Int => Int, g: Int => Int) => f(g(n))
    
  
  //(f) Curried version of (e)
  
  val f = (n: Int) => (f: Int => Int) => (g: Int => Int) => f(g(n))
  
  /**
   * Question 1 - Run
   */
  
  println(a(true))
  
  println(b(3))
  
  println(c(4, (x)=> x*x))
  
  println(d(4)((x)=> (x*x)))
  
  println(e(3, (x => (x*x*x)), (x => x * x)))
  
  println(f (3) (x => x*x*x) (x => x *x))
  
  
  
  
  /**
   * Question 2 - Answers
   */
  
  //(a) Write an expression that creates a List containing the 
  //integers 1 through 5, in order.
  
  val list1 = List(1,2,3,4,5)
  
  //(b) Write an expression that reverses the List in (a).
  
  println(list1.reverse)
  
  //(c) Write an expression that returns a new List given by negating every element of the list in ( a).
  println(list1 map((n => -n)))
  
  //(d) Write an expression which sums the elements of the List in (c).
  
  
  println(list1.foldLeft(0)((b,a) => b+a))
  //or
  println(list1.foldLeft(0)(_+_))
  // multiplication, note the 1 instead of 0
  println(list1.foldLeft(1)((b,a) => b*a))
  
  
 
  
  
  }
}