object BasicSyntax2 {

  def main(args: Array[String]){
    /**
     * Function definition
     */
    
    def isEven (n: Int) = {
      val m  = n % 2
      m == 0
    }
    
    //good style to omit = when the result is ()
    def countTo(n: Int){
      for(i <- 1 to n){
    //    println(i)
      }
    }
    
    countTo(5)
    
    def half(n: Int): Double = n /2
    
   // println(half(11))
    
    /**
     * Functions as first class objects
     */
    
    // paramater_list => function_biody
    
    val foo = (x: Int) => if (x % 2 == 0) x / 2 else 3 * x + 1
  //  println(foo(3))
    
    val mylist1 = List(1,2,3,4)
   // mylist1.foreach(i => println(i*i))
    
    
    /**
     * Functions as parameters
     */
    
    def doTwice(f: Int => Int, n: Int) = f(f(n))
   
   // println(doTwice(a => 101 * a, 3))
    
    
    
    
    /**
     * Higher order functions
     */
  
    val list2 = List(2,4,6,7,8,9)
    
    //println(list2 map (x => x * x))
    
   // println( list2 map (n => n > 7))
    
    
    
    /**
     * Pattern matching
     */
    
    //on literal values
    /*
    "Monday" match{
      case "Monday" => println("I hate the start of the week")
      case "Friday" => println("party tonight")
    }
    */
    
    /**
     * The option type
     */
    
    
    val jList = List(1,2,3,4)
    
    def max(list: List[Int]) = {
      if (list.length > 0) {
        val biggest = (list(0) /: list)
          {(a, b) => if (a > b) a else b }
        Some(biggest)
      } else {
        None
     }
     
      /* NEEDS FIXING
      max(jList) match {
        case Some(x) => println("The largest number is " + x)
        case None => println("There are no numbers here!!!")
      }
      */
      
    }
      
  }
}